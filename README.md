#Texting and its Effect on the Culture of Language#

**Points against texting** 

- What I mean by the culture of language is a face to face conversation 
- The technology of texting has made it a norm to not interact directly with the person you are speaking to
- Rather than having a conversation with someone people send one or two texts and consider that a conversarion 
- There is no face to face interaction 

**Texting has altered the way human's speak to one another on a daily basis**

- People have begun to use text lingo such as "lol" in their conversations instead of actually laughing 

**Shiviro says "Every connection has its price: the one you can be sure of is that, sooner or later you will have to pay." (3)**

- The price of texting is that we are losing our language.
- It has become a cultural norm to not have face to face conversations 
	- Or even vocal conversations 
	
**Personal Example**
		- I rarely call my mother on the phone. Often times I consider a short discussion over text to be a genuine conversation 

**Millenial Example**
	-* 68% of 18- to 29-year-olds say that they texted �a lot� the previous day, which plunges to 47% among 30- to 49-year-olds and 26% among 50- to 64-year-olds.*
	<https://www.forbes.com/sites/neilhowe/2015/07/15/why-millennials-are-texting-more-and-talking-less/#cd269bb59752> *
	
![caption](https://i1.wp.com/www.textrequest.com/blog/wp-content/uploads/2016/05/Daily-Monthly-Texts-e1477505556832.png?ssl=1)